$(document).ready(function() {

  $(window).load(function() {
    $("#content-loader").fadeOut(1000);
  });

  $("#view-link").hover(
    function() {
      $("#part0").addClass("blur");
    },

    function() {
      $("#part0").removeClass("blur");
    }
  );

  $("#main-link").hover(
    function() {
      $("#part1").addClass("blur");
    },

    function() {
      $("#part1").removeClass("blur");
    }
  );

  $("#view-link").click(function() {
    $("#part0, #view-link").fadeOut(1000);
    $("#part1").fadeIn(1000);
    $("#main-link").delay(3000).fadeIn(500);
  });

  $("#main-link").click(function() {
    $("#part1, #main-link").fadeOut(1000);
    $("#part0").fadeIn(1000);
    $("#view-link").delay(3000).fadeIn(500);
  });

  $(".fancybox").fancybox({
    closeBtn  : false,
    arrows    : true,
    nextClick : false,
    mouseWheel: true,

    tpl: {
      error: '<p class="fancybox-error">Невозможно загрузить изображение.<br/>Повторите попытку позже.<br/>Если ошибка будет повторяться,<br/>вы можете написать на <a href="mailto:zvorygin@yararchive.ru?subject=Проблемы с показом изображений в электронной выставке К 70-летию Победы советского народа над фашистской Германией. Письма с фронта.">zvorygin@yararchive.ru</a></p>'
    },

    helpers : {
      thumbs : {
        width  : 50,
        height : 50
      }
    }
  });
});